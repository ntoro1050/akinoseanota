--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.12
-- Dumped by pg_dump version 9.3.12
-- Started on 2016-05-13 17:58:10 VET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 2082 (class 1262 OID 17182)
-- Name: Akinoseanota_db; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE "Akinoseanota_db" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'es_VE.UTF-8' LC_CTYPE = 'es_VE.UTF-8';


ALTER DATABASE "Akinoseanota_db" OWNER TO postgres;

\connect "Akinoseanota_db"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 1 (class 3079 OID 11829)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2085 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 177 (class 1259 OID 17217)
-- Name: agreements; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE agreements (
    id integer NOT NULL,
    title character varying NOT NULL,
    description text NOT NULL,
    issue_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.agreements OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 17215)
-- Name: agreements_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE agreements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agreements_id_seq OWNER TO postgres;

--
-- TOC entry 2086 (class 0 OID 0)
-- Dependencies: 176
-- Name: agreements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE agreements_id_seq OWNED BY agreements.id;


--
-- TOC entry 181 (class 1259 OID 17251)
-- Name: guests; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE guests (
    id integer NOT NULL,
    name character varying NOT NULL,
    last_name character varying NOT NULL,
    email character varying NOT NULL,
    phone character varying NOT NULL,
    task_id integer,
    meeting_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.guests OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 17249)
-- Name: guests_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE guests_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.guests_id_seq OWNER TO postgres;

--
-- TOC entry 2087 (class 0 OID 0)
-- Dependencies: 180
-- Name: guests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE guests_id_seq OWNED BY guests.id;


--
-- TOC entry 175 (class 1259 OID 17203)
-- Name: issues; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE issues (
    id integer NOT NULL,
    estimated_duration time without time zone NOT NULL,
    meeting_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.issues OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 17201)
-- Name: issues_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE issues_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.issues_id_seq OWNER TO postgres;

--
-- TOC entry 2088 (class 0 OID 0)
-- Dependencies: 174
-- Name: issues_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE issues_id_seq OWNED BY issues.id;


--
-- TOC entry 173 (class 1259 OID 17192)
-- Name: meetings; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE meetings (
    id integer NOT NULL,
    "time" time without time zone NOT NULL,
    date date NOT NULL,
    place text NOT NULL,
    matter text NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.meetings OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 17190)
-- Name: meetings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE meetings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.meetings_id_seq OWNER TO postgres;

--
-- TOC entry 2089 (class 0 OID 0)
-- Dependencies: 172
-- Name: meetings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE meetings_id_seq OWNED BY meetings.id;


--
-- TOC entry 171 (class 1259 OID 17183)
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 17234)
-- Name: tasks; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tasks (
    id integer NOT NULL,
    description text NOT NULL,
    data_to_completed date NOT NULL,
    agreement_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.tasks OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 17232)
-- Name: tasks_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tasks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tasks_id_seq OWNER TO postgres;

--
-- TOC entry 2090 (class 0 OID 0)
-- Dependencies: 178
-- Name: tasks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tasks_id_seq OWNED BY tasks.id;


--
-- TOC entry 1936 (class 2604 OID 17220)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY agreements ALTER COLUMN id SET DEFAULT nextval('agreements_id_seq'::regclass);


--
-- TOC entry 1938 (class 2604 OID 17254)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY guests ALTER COLUMN id SET DEFAULT nextval('guests_id_seq'::regclass);


--
-- TOC entry 1935 (class 2604 OID 17206)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY issues ALTER COLUMN id SET DEFAULT nextval('issues_id_seq'::regclass);


--
-- TOC entry 1934 (class 2604 OID 17195)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY meetings ALTER COLUMN id SET DEFAULT nextval('meetings_id_seq'::regclass);


--
-- TOC entry 1937 (class 2604 OID 17237)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tasks ALTER COLUMN id SET DEFAULT nextval('tasks_id_seq'::regclass);


--
-- TOC entry 2073 (class 0 OID 17217)
-- Dependencies: 177
-- Data for Name: agreements; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2091 (class 0 OID 0)
-- Dependencies: 176
-- Name: agreements_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('agreements_id_seq', 1, false);


--
-- TOC entry 2077 (class 0 OID 17251)
-- Dependencies: 181
-- Data for Name: guests; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2092 (class 0 OID 0)
-- Dependencies: 180
-- Name: guests_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('guests_id_seq', 1, false);


--
-- TOC entry 2071 (class 0 OID 17203)
-- Dependencies: 175
-- Data for Name: issues; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2093 (class 0 OID 0)
-- Dependencies: 174
-- Name: issues_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('issues_id_seq', 1, false);


--
-- TOC entry 2069 (class 0 OID 17192)
-- Dependencies: 173
-- Data for Name: meetings; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2094 (class 0 OID 0)
-- Dependencies: 172
-- Name: meetings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('meetings_id_seq', 1, false);


--
-- TOC entry 2067 (class 0 OID 17183)
-- Dependencies: 171
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO schema_migrations (version) VALUES ('20160513013239');
INSERT INTO schema_migrations (version) VALUES ('20160513013417');
INSERT INTO schema_migrations (version) VALUES ('20160513013450');
INSERT INTO schema_migrations (version) VALUES ('20160513013723');
INSERT INTO schema_migrations (version) VALUES ('20160513013820');


--
-- TOC entry 2075 (class 0 OID 17234)
-- Dependencies: 179
-- Data for Name: tasks; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2095 (class 0 OID 0)
-- Dependencies: 178
-- Name: tasks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tasks_id_seq', 1, false);


--
-- TOC entry 1946 (class 2606 OID 17225)
-- Name: agreements_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY agreements
    ADD CONSTRAINT agreements_pkey PRIMARY KEY (id);


--
-- TOC entry 1952 (class 2606 OID 17259)
-- Name: guests_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY guests
    ADD CONSTRAINT guests_pkey PRIMARY KEY (id);


--
-- TOC entry 1944 (class 2606 OID 17208)
-- Name: issues_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY issues
    ADD CONSTRAINT issues_pkey PRIMARY KEY (id);


--
-- TOC entry 1941 (class 2606 OID 17200)
-- Name: meetings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY meetings
    ADD CONSTRAINT meetings_pkey PRIMARY KEY (id);


--
-- TOC entry 1950 (class 2606 OID 17242)
-- Name: tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (id);


--
-- TOC entry 1947 (class 1259 OID 17226)
-- Name: index_agreements_on_issue_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_agreements_on_issue_id ON agreements USING btree (issue_id);


--
-- TOC entry 1953 (class 1259 OID 17261)
-- Name: index_guests_on_meeting_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_guests_on_meeting_id ON guests USING btree (meeting_id);


--
-- TOC entry 1954 (class 1259 OID 17260)
-- Name: index_guests_on_task_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_guests_on_task_id ON guests USING btree (task_id);


--
-- TOC entry 1942 (class 1259 OID 17209)
-- Name: index_issues_on_meeting_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_issues_on_meeting_id ON issues USING btree (meeting_id);


--
-- TOC entry 1948 (class 1259 OID 17243)
-- Name: index_tasks_on_agreement_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_tasks_on_agreement_id ON tasks USING btree (agreement_id);


--
-- TOC entry 1939 (class 1259 OID 17189)
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- TOC entry 1958 (class 2606 OID 17262)
-- Name: fk_rails_a7ab98e12f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY guests
    ADD CONSTRAINT fk_rails_a7ab98e12f FOREIGN KEY (task_id) REFERENCES tasks(id);


--
-- TOC entry 1957 (class 2606 OID 17244)
-- Name: fk_rails_a83f70d307; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tasks
    ADD CONSTRAINT fk_rails_a83f70d307 FOREIGN KEY (agreement_id) REFERENCES agreements(id);


--
-- TOC entry 1956 (class 2606 OID 17227)
-- Name: fk_rails_ad83f4cb3b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY agreements
    ADD CONSTRAINT fk_rails_ad83f4cb3b FOREIGN KEY (issue_id) REFERENCES issues(id);


--
-- TOC entry 1959 (class 2606 OID 17267)
-- Name: fk_rails_bd398abcf6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY guests
    ADD CONSTRAINT fk_rails_bd398abcf6 FOREIGN KEY (meeting_id) REFERENCES meetings(id);


--
-- TOC entry 1955 (class 2606 OID 17210)
-- Name: fk_rails_f5ae366250; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY issues
    ADD CONSTRAINT fk_rails_f5ae366250 FOREIGN KEY (meeting_id) REFERENCES meetings(id);


--
-- TOC entry 2084 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-05-13 17:58:11 VET

--
-- PostgreSQL database dump complete
--

