class Meeting < ActiveRecord::Base
	has_many :issues
	has_many :guests
end
