class Agreement < ActiveRecord::Base
  belongs_to :issue
  has_many :tasks
end
