class Task < ActiveRecord::Base
  belongs_to :agreement
  has_many :guests
end
