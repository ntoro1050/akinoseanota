require 'valid_email'
class Guest < ActiveRecord::Base
	include ActiveModel::Validations

  belongs_to :task
  belongs_to :meeting

	validates :email, email: true
	validates_presence_of :name, :phone, :last_name, :meeting
	validates_uniqueness_of :phone

end
