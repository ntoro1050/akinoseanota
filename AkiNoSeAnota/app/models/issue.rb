class Issue < ActiveRecord::Base
  belongs_to :meeting
  has_many :agreements
end
