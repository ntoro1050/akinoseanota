class CreateGuests < ActiveRecord::Migration
  def change
    create_table :guests do |t|
      t.string :name, null: false
      t.string :last_name, null: false
      t.string :email, null: false
      t.string :phone, null: false
      t.references :task, index: true, foreign_key: true
      t.references :meeting, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
