class CreateAgreements < ActiveRecord::Migration
  def change
    create_table :agreements do |t|
      t.string :title, null: false
      t.text :description, null: false
      t.references :issue, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
