class CreateMeetings < ActiveRecord::Migration
  def change
    create_table :meetings do |t|
      t.time :time, null: false
      t.date :date, null: false
      t.text :place, null: false
      t.text :matter, null: false

      t.timestamps null: false
    end
  end
end
