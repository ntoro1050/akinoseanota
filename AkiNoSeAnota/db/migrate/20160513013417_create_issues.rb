class CreateIssues < ActiveRecord::Migration
  def change
    create_table :issues do |t|
      t.time :estimated_duration, null: false
      t.references :meeting, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
