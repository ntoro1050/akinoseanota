class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.text :description, null: false
      t.date :data_to_completed, null: false
      t.references :agreement, index: true, foreign_key: true, null: false

      t.timestamps null: false
    end
  end
end
