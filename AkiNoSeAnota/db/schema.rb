# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160513013820) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "agreements", force: :cascade do |t|
    t.string   "title",       null: false
    t.text     "description", null: false
    t.integer  "issue_id",    null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "agreements", ["issue_id"], name: "index_agreements_on_issue_id", using: :btree

  create_table "guests", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "last_name",  null: false
    t.string   "email",      null: false
    t.string   "phone",      null: false
    t.integer  "task_id"
    t.integer  "meeting_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "guests", ["meeting_id"], name: "index_guests_on_meeting_id", using: :btree
  add_index "guests", ["task_id"], name: "index_guests_on_task_id", using: :btree

  create_table "issues", force: :cascade do |t|
    t.time     "estimated_duration", null: false
    t.integer  "meeting_id",         null: false
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "issues", ["meeting_id"], name: "index_issues_on_meeting_id", using: :btree

  create_table "meetings", force: :cascade do |t|
    t.time     "time",       null: false
    t.date     "date",       null: false
    t.text     "place",      null: false
    t.text     "matter",     null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tasks", force: :cascade do |t|
    t.text     "description",       null: false
    t.date     "data_to_completed", null: false
    t.integer  "agreement_id",      null: false
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "tasks", ["agreement_id"], name: "index_tasks_on_agreement_id", using: :btree

  add_foreign_key "agreements", "issues"
  add_foreign_key "guests", "meetings"
  add_foreign_key "guests", "tasks"
  add_foreign_key "issues", "meetings"
  add_foreign_key "tasks", "agreements"
end
